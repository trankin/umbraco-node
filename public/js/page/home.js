define("page_home", ['socket', 'jquery', 'underscore'], function(socket, $, _){

	  var tweet_template = _.template($("#tweet_template").html());

	  var displayTweet = function(tweet){
		if(tweet && tweet.text)
	  	{
	  		$("#tweet_stream").prepend(tweet_template(tweet));
	  		$("#t_" + tweet.id_str).hide().slideDown();
	  		$( "li:gt(20)" ).remove();
	  	}
		console.log(tweet);
	  };


	  socket.emit("twitter/latest", "");

	  socket.on('twitter/catchup', function (data) {
	  	for(var i = 0; i < data.length; i++) {
	  		//console.log(data[i]);
	  		displayTweet(data[i]);
	  	}
	  });	

	  socket.on('twitter/stream', function (data) {
	  	displayTweet(data);
	  });	



});