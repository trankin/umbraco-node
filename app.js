#!/usr/bin/env node
var debug = require('debug')('my-application');
var server = require('./server');

server.set('port', (process.env.HOST && process.env.HOST === "api.bowery.io") ? 80 : (process.env.PORT || 3000));

var listener = server.listen(server.get('port'), function () {
    'use strict';
    debug('Express server listening on port ' + listener.address().port);
    console.log('Express server listening on port ' + listener.address().port);
});

//Initialize the socket/// I can probably move this
//into the application
require("./application/core/socket")(listener);
require("./application");





