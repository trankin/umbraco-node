var db = require("../models");
var async = require("async");

exports.actions = {
    descendants: function(req, res) {
        db.DocumentType.get(req.params.id, function (err, doc) {

            async.parallel([
                function(callback) {
                    doc.descendants(function(err, docs) {
                        callback(err, docs);
                    });
                },
                function(callback) {
                    doc.allFields(function(err, fields) {
                        callback(err, fields);
                    });
                },
            ], function(err, results){
                if(!err) {
                    res.render("documenttype/detail", {
                        doc: doc,
                        descendants: results[0],
                        fields: results[1]
                    });
                } else {
                    res.render("shared/error", err);
                }
            });
            
        });

    },

    index: function(req, res) {
        exports.actions.list(req, res);
    },

    test: function(req, res) {
        var i = 0;
        var doc;
        var docs = [];

        for(i = 0; i < 200; i++)
        {
            docs.push(new db.DocumentType({
                name : "Base" + i,
                alias : "base" + i,
                parentAlias: i === 0 ? "base" : "base" + (Math.floor(Math.random() * 200) + 1),
                fields : [
                    {
                        alias : "title",
                        label : "Title",
                        dataTypeAlias : "textString",
                        sortOrder : 0
                    }
                ]
            }));

        }


        async.eachSeries(docs, function (item, callback) {
            db.documenttypes.save(item, callback);
        }, function (err, results) {
            if (!err) {
                res.send("DONE");
            } else {
                res.send(err.toString());
            }
            
        });

    },

    init: function (req, res) {
        'use strict';

        var documentTypeList = [
            new db.DocumentType({
                name : "Root",
                alias : "root",
                parentAlias: ""
            }),
            new db.DocumentType({
                name : "Base",
                alias : "base",
                parentAlias: "root",
                fields : [
                    {
                        alias : "title",
                        label : "Title",
                        dataTypeAlias : "textString",
                        sortOrder : 0
                    }
                ]
            }),
            new db.DocumentType({
                name : "Text Page",
                alias : "textPage",
                parentAlias: "base",
                fields : [
                    {
                        alias : "content",
                        label : "Content",
                        dataTypeAlias : "richText",
                        sortOrder : 1
                    }
                ]
            })
        ];

        async.eachSeries(documentTypeList, function (item, callback) {
            db.DocumentType.save(item, callback);
        }, function (err, results) {
            if (!err) {
                exports.actions.list(req, res);
            } else {
                res.send(err.toString());
            }
            
        });
    },

    list: function (req, res) {
        'use strict';
        db.DocumentType.find(function (err, docs) {
            res.send(docs);
        });
    },
};