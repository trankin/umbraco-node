var app = require("../../application");

exports.actions = {

    index: function (req, res) {
        'use strict';
        res.render("home/index", {});
    }

};


exports.socketActions = {

    index: function (socket, event, data) {
        'use strict';
        socket.emit("message", { message: "Hello, Welcome"});
    }

};