var db = require("../models");
var async = require("async");

exports.actions = {
    index: function(req, res) {
        this.list(req, res);
    },

    list: function (req, res) {
        'use strict';
        db.DataType.find(function (err, dataTypes) {
            res.send(dataTypes);
            //res.render('datatype/list', {dataTypes: dataTypes});
        });
    },

    init: function (req, res) {
        'use strict';

        var dataTypeList = [
            {
                name : "Text String",
                alias : "textString",
                propertyEditor : "textStringEditor",
                propertyType : "String"
            },

            {
                name : "Date Picker",
                alias : "datePicker",
                propertyEditor : "datePickerEditor",
                propertyType : "DateTime"
            },

            {
                name : "True/False",
                alias : "trueFalse",
                propertyEditor : "trueFalseEditor",
                propertyType : "Boolean"
            },

            {
                name : "Rich Text",
                alias : "richText",
                propertyEditor : "richTextEditor",
                propertyType : "String"
            }
        ];

        async.each(dataTypeList, function (item, callback) {
            db.DataType.upsert(item, callback);
        }, function (err) {
            res.send(err);
        });
    }
};