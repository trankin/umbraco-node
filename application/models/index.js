var fs = require("fs");
var path = require("path");
var debug = require("debug")("models");
var pluralize = require("pluralize");

//Register Models
(function () {
    'use strict';
    var mongoose = require("mongoose");

    fs.readdir("./application/models", function (err, files) {
        var i, name;

        for (i in files) {
            name = path.basename(files[i], ".js");
            if(name !== "index") {
                var item = require("./" + files[i]);
                module.exports[name] = item.model;
            }
        }


        if (process.env.DATABASE_ADDR) {
            mongoose.connect(process.env.DATABASE_ADDR + '/db');
        } else {
            mongoose.connect('mongodb://localhost/umbracotest');
        }
    });
}());