var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var nestedSet = require('../core/nested_set');
var Schema   = mongoose.Schema;

var documentSchema = new Schema({
    name : { type: String, required: true },
    alias : { type: String, required: true, index: true, unique: true },
    documentTypeAlias : { type: String, required: true, index: true },
    documentTypes : { type: String, required: true },
    documentType : { type: Schema.ObjectId, ref: 'DocumentType' },
    path : { type: String },
    updated_on : { type: Date, required: true, default: Date.now() },
    created_on : { type: Date, required: true, default: Date.now() },
    content : { type: Schema.Types.Mixed }
});

documentSchema.plugin(uniqueValidator);
documentSchema.plugin(nestedSet);

mongoose.model("Document", documentSchema);
var model = mongoose.model("Document");

/*============= EXPORTS =============== */

exports.model = model;

exports.set = {


};






