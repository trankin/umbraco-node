var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var DataType;


var dataTypeSchema = new Schema({
    name : { type: String, required: true },
    alias : { type: String, required: true, index: true, unique: true },
    propertyEditor : { type: String, required: true },
    propertyType : { type: String, required: true, enum: ['String', 'Boolean', 'Number', 'Date', 'Array'] },
    updated_on : { type: Date, required: true, default: Date.now() },
    created_on : { type: Date, required: true, default: Date.now() }
});


dataTypeSchema.index({alias: 1});

dataTypeSchema.statics.upsert = function(dataType, callback) {
    var self = this;
    self.update({alias: dataType.alias}, dataType, {upsert: true}, function (err, numAffected) {
        callback(err, numAffected);
    });
};


mongoose.model("DataType", dataTypeSchema);
dataTypeSchema.plugin(uniqueValidator);

var model = exports.model = mongoose.model("DataType");



