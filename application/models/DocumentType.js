'use strict';

var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var nestedSet = require('../core/nested_set');
var async = require("async");
var Schema   = mongoose.Schema;

var fieldSchema = new Schema({
    alias : String,
    sortOrder : Number,
    required : Boolean,
    regex : String,
    customValidator : String,
    dataTypeAlias : String,
    label: String,
    dataType : { type: Schema.ObjectId, ref: 'DataType' }
});


var childrenSchema = new Schema({
    sortOrder : Number,
    documentType : { type: Schema.ObjectId, ref: 'DocumentType' }
});

var documentTypeSchema = new Schema({
    name : { type: String, required: true },
    alias : { type: String, required: true, index: true, unique: true },
    parentAlias : { type: String },
    path : [String],
    documentTypes : { type: String, required: true },
    fields : { type: [fieldSchema] },
    parentId : { type: Schema.ObjectId, ref: 'DocumentType' },
    ancestorIds : [String],
    updated_on : { type: Date, required: true, default: Date.now() },
    created_on : { type: Date, required: true, default: Date.now() }
});

documentTypeSchema.index({parentId: 1});
documentTypeSchema.index({alias: 1});
documentTypeSchema.index({path: 1});
documentTypeSchema.index({ancestorIds: 1});

documentTypeSchema.statics.get = function (alias, callback) {
    var self = this;
    self.findOne({ alias: alias }, function (err, doc) {
        callback(err, doc);
    });
};

documentTypeSchema.statics.save = function (documentType, callback) {
    var self = this;
    self.get(documentType.alias, function (err, existingDoc) {
        if (!err) {
            // If the doc already exists, then maybe setting the id to the existing
            // will force an update instead of a new???/  we'll find out.
            // Ok, so nope.. it doesn't just save the existin
            if (existingDoc) {
                documentType._id = existingDoc._id;
            }

            async.parallel([
                function (callback) {
                    documentType.updateParentRefs(function (err) {
                        callback(err);
                    });
                }
            ], function (err, results) {
                if (!err) {
                    if (existingDoc) {

                        documentType = documentType.toObject();
                        delete documentType._id;

                        self.update({alias: documentType.alias}, documentType, {upsert: true}, function (err, numAffected) {
                            callback(err, documentType);
                        });
                    } else {
                        documentType.save(function (err, doc) {
                            callback(err, doc);
                        });
                    }
                }
            });
        } else {
            callback(err);
        }
    });
};


documentTypeSchema.methods.allFields = function (callback) {
    var self = this;
    self.ancestorsAndSelf(function (err, docs) {
        async.reduce(docs, [], function (memo, doc, callback) {
            process.nextTick(function () {
                callback(null, memo.concat(doc.fields));
            });
        }, function (err, result) {
            console.log(err);
            console.log(result);
            callback(err, result);
        });
    });
}

documentTypeSchema.methods.updateParentRefs = function(callback) {
    var self = this;

    if (!self.parentAlias || self.parentAlias === "") {
        if (self.alias !== "root") {
            self.parentAlias = "root";
        }

    }

    if (self.parentAlias && self.parentAlias !== "") {

        self.constructor.get(self.parentAlias, function (err, doc) {
            if (!err && doc) {

                self.path = doc.path;
                self.path.push(doc._id);
                
                self.parentId = doc._id;
                self.documentTypes = doc.documentTypes + self.alias + ",";
                self.ancestorIds = doc.ancestorIds;
                self.ancestorIds.push(doc._id);
            }
            if(!doc) console.log(JSON.stringify(self));
            callback(err);
            return;
        });

    } else {
        self.path = [];
        self.parentId = null;
        self.documentTypes = "," + self.alias + ",";
        self.ancestorIds = [];
        callback(null);
        return;
    }
};



documentTypeSchema.plugin(uniqueValidator);
documentTypeSchema.plugin(nestedSet);
mongoose.model("DocumentType", documentTypeSchema);
var model = exports.model = mongoose.model("DocumentType");


