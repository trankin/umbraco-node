var server = require("../server");
var fs = require("fs");
var path = require("path");
var debug = require("debug")("application-index");

//Register Controllers + Socket Handlers
exports.db = require("./models");

(function () {
    'use strict';
    var controllers = {};
    var io = require("./core/socket");

    fs.readdir("./application/controllers", function (err, files) {
        var i, name, controller;
        for (i in files) {
            name = path.basename(files[i], ".js");
            controllers[name] = {};
            controller = require("./controllers/" + files[i]);
            controllers[name].actions = controller.actions || [];
            controllers[name].socketActions = controller.socketActions || [];

            debug("controller " + name + ": registered");
        }

        exports.controllers = controllers;
    });



    var actionHandler = function (req, res) {
        var controller = (req.params.controller || "home").toLowerCase();
        var action = (req.params.action || "index").toLowerCase();

        if (controllers[controller] && controllers[controller].actions[action]) {
            controllers[controller].actions[action](req, res);
        } else {
            fs.exists("./application/views/" + controller + "/" + action + ".vash", function (exists) {
                if (exists) {
                    res.render(controller + "/" + action, {});
                } else {
                    var err = new Error('Not Found');
                    err.status = 404;
                    res.statusCode = 404;
                    res.render('shared/error', {
                        message: err.message,
                        req: req,
                        error: {}
                    });
                }
            });

        }
    };


    var socketActionHandler = function (socket, event, data) {
        var args = event.split('/'),
            params = {
                controller: args.length > 0 ? args[0].toLowerCase() : "home",
                action: args.length > 1 ? args[1].toLowerCase() : "index"
            };

        if (controllers[params.controller] && controllers[params.controller].socketActions[params.action]) {
            controllers[params.controller].socketActions[params.action](socket, event, data);
        } else {
            //Should send back an error.
            socket.emit(event, data);
        }
    };

    server.get('/:controller?/:action?/:id?', actionHandler);



    io.sockets.on('connection', function (socket) {
        // set up the bindings
        socket.on('*', function (event, data) {
            socketActionHandler(socket, event.data[0], data);
        });
    });


    exports.actionHander = actionHandler;


}());

