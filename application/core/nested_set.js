/*globals require, console, module */
'use strict';
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    async = require('async');

var NestedSetPlugin = function(schema, options) {
  options = options || {};

  // Returns true if the node is a leaf node (i.e. has no children)
  schema.method('isLeaf', function() {
    self.constructor.where('parent').eq(self._id).exec(function(err, docs){
      if(!err) {
        return docs ? true : false;
      } else {
        return false;
      }

    });
  });

  // Returns true if the node is a child node (i.e. has a parent)
  schema.method('isChild', function() {
    return !!this.parent;
  });

  // Returns true if other is a descendant of self
  schema.method('isDescendantOf', function(other) {
    var self = this;
    return self.path.indexOf(other._id) > -1;
  });

  // Returns true if other is an ancestor of self
  schema.method('isAncestorOf', function(other) {
    var self = this;
    return other.path.indexOf(self._id) > -1;
  });

  // returns the parent node
  schema.method('parent', function(callback) {
    var self = this;
    self.constructor.findOne({_id: self.parentId}, callback);
  });

  // Returns the list of ancestors + current node
  schema.method('ancestorsAndSelf', function(callback) {
    var self = this;
    if(!self._ancestorsAndSelf) {
      self.ancestors(function(err, nodes) {
        if (err) {
          callback(err, null);
        } else {
          self._ancestorsAndSelf = nodes.concat([self]);
          callback(null, self._ancestorsAndSelf);
        }
      });
    } else {
      callback(null, self._ancestorsAndSelf);
    }
  });

  // Returns the list of ancestors
  schema.method('ancestors', function(callback) {
    var self = this;

    if(!self._ancestors) {
      self.constructor.find({_id: {$in: self.ancestorIds}}).exec(function(err, ancestors){
        if(!err) {
          self._ancestors = ancestors;
          callback(null, self._ancestors);
        } else {
          callback(err, null);
        }
      });
    } else {
      callback(null, self._ancestors);
    }
  });

  // Returns the list of children
  schema.method('children', function(callback) {
    var self = this;
    if(!self._children) {
      self.constructor.find({parent: self._id}, function(err, children){
        if(!err) {
          self._children = children;
          callback(null, self._children);
        } else {
          callback(err, null);
        }
      });
    } else {
      callback(null, self._children);
    }
    
  });

  // Returns the list of children + current node
  schema.method('childrenAndSelf', function(callback) {
    var self = this;
    self.children(function(err, nodes) {
      if (err) {
        callback(err, null);
      } else {
        callback(null, nodes.concat([self]));
      }
    });
  });

  // Returns the list of descendants + current node
  schema.method('descendantsAndSelf', function(callback) {
    var self = this;
    if(!self._descendantsAndSelf) {
      self.descendants(function(err, nodes) {
        if (err) {
          callback(err, null);
        } else {
          self._descendantsAndSelf = nodes.concat([self]);
          callback(null, self._descendantsAndSelf);
        }
      });
    } else {
        callback(null, self._descendantsAndSelf);
    }
  });

  // Returns the list of descendants
  schema.method('descendants', function(callback) {
    var self = this;
    if(!self._descendantsAndSelf) {
      self.constructor.find({path: self._id }).exec(function(err, docs){
          if(err) {
            callback(err, null);
          } else {
              self._descendants = docs;
              callback(null, self._descendants);
          }
      });
    } else {
      callback(null, self._descendants);
    }
  });

  // Returns the list of all nodes with the same parent + current node
  schema.method('siblingsAndSelf', function(callback) {
    var self = this;
    self.constructor.find({parentId: self.parentId}, callback);
  });

  // Returns the list of all nodes with the same parent
  schema.method('siblings', function(callback) {
    var self = this;
    self.siblingsAndSelf(function(err, nodes) {
      if (err) {
        callback(err, null);
      } else {
        var nodesMinusSelf = nodes.filter(function(node) { return self._id.toString() !== node._id.toString(); });
        callback(null, nodesMinusSelf);
      }
    });
  });

  // Returns the level of this object in the tree. Root level is 0
  schema.method('level', function() {
    return this.ancestorIds.length;
  });


};

module.exports = exports = NestedSetPlugin;
