var io = require("socket.io");

module.exports = function (listener) {
    'use strict';
    io = io(listener);
    io.use(require("socketio-wildcard")());
    module.exports = io;
};