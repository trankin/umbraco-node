var express = require('express');
var server = express();
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var debug = require("debug")("server");

// view engine setup
server.set('views', path.join(__dirname, 'application/views'));
server.set('view engine', 'vash');

server.use(express.compress());
server.use(favicon());
server.use(logger('dev'));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded());
server.use(cookieParser());
server.use(express.static(path.join(__dirname, 'public')));
server.use(server.router);

/// catch 404 and forwarding to error handler
server.use(function (req, res, next) {
    'use strict';
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (server.get('env') === 'development') {
    server.use(function (err, req, res, next) {
        'use strict';
        res.render('shared/error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
server.use(function (err, req, res, next) {
    'use strict';
    res.render('shared/error', {
        message: err.message,
        error: {}
    });
});


module.exports = server;
